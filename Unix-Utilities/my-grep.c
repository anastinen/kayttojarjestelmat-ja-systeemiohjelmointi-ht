/* Antti Vilkman 0521281
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * 12/2018
 * Harjoitustyö Unix-Utilities
 * my-grep.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char *line = NULL; 
	size_t linecap = 0;
	FILE *f;
	// If no input has been given, exit the program
	if (argc == 1) {
		printf("my-grep: searchterm [file ...]\n");
		exit(1);
	}

	// If no file has been given, read from stdin
	if (argc == 2) {
		getline(&line, &linecap, stdin);
		if (strstr(line, argv[1])) {
			printf("%s", line);
		}
		return 0;
	} 
	else { // If given file can't be accessed, exit
		f = fopen(argv[2], "r");
		if (!f) {
			printf("my-grep: cannot open file\n");
			exit(1);
		}
	}

	// Print lines which contain the given string
	while(!feof(f)) {
		getline(&line, &linecap, f);
		if (strstr(line, argv[1])) {
			printf("%s\n", line);
		}
	}
	fclose(f);
	return 0;
}
