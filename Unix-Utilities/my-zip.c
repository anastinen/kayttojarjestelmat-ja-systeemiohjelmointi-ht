/* Antti Vilkman 0521281
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * 12/2018
 * Harjoitustyö Unix-Utilities
 * my-zip.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	FILE *f;
	// If no input has been given, exit the program
	if (argc == 1) {
		printf("my-zip: file1 [file2 ...]\n");
		exit(1);
	}

	else { // If given file can't be accessed, exit
		f = fopen(argv[1], "r");
		if (!f) {
			printf("my-zip: cannot open file\n");
			exit(1);
		}
	}

	// Move through the file counting the run of each charecter and write it into the file
	char current = getc(f);
	char temp;
	int counter = 1;
	while(!feof(f)) {
		if ((temp = getc(f)) == current) {
			counter++;
		}
		else {
			fwrite(&counter, sizeof(int), 1, stdout);
			fwrite(&current, sizeof(char), 1, stdout);
			current = temp;
			counter = 1;
		}
	}
	fclose(f);
	printf("\n");
	return 0;
}