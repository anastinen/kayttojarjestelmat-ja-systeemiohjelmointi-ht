/* Antti Vilkman 0521281
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * 12/2018
 * Harjoitustyö Unix-Utilities
 * my-cat.c
 */


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	char buffer[1000]; 

	// If no input has been given, exit the program
	if (argc == 1) {
		return 0;
	}
	// Loop through each file 
	for (int i=1; i<argc; i++) {
		FILE *f = fopen(argv[i], "r");

		if (!f) {
			printf("my-cat: cannot open file\n");
			exit(1);
		}
		// Print lines until end of file is reached
		while(fgets(buffer, 1000, f)) {
			printf("%s", buffer);
		}
		fclose(f);
	}
	return 0;
}
