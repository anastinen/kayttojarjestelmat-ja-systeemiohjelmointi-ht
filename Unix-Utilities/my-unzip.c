/* Antti Vilkman 0521281
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * 12/2018
 * Harjoitustyö Unix-Utilities
 * my-unzip.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	FILE *f;
	// If no input has been given, exit the program
	if (argc == 1) {
		printf("my-unzip: file1 [file2 ...]\n");
		exit(1);
	}

	else { // If given file can't be accessed, exit
		f = fopen(argv[1], "r");
		if (!f) {
			printf("my-unzip: cannot open file\n");
			exit(1);
		}
	}

	// Uncompress the file
	char temp;
	int counter = 0;
	while(!feof(f)) {
		fread(&counter, sizeof(int), 1, f);
		fread(&temp, sizeof(char), 1, f);
		for (int i=0; i<counter; i++) {
			printf("%c", temp);
		}
		counter = 0;
		temp = '\0';
	}
	fclose(f);
	return 0;
}